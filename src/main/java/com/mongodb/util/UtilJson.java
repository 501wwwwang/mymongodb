/*
 * 文 件 名:  UtilJson.java
 * 版    权:  Chrhc Technologies Co., Ltd. Copyright 2015-2099,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人: 姓名 xxx
 * 修改时间:  2015年11月25日
 */
package com.mongodb.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * json公用
 * 
 * @author 姓名 王晨
 * @version [1.0, 2015年11月25日]
 */
public class UtilJson
{
    
    // private static JsonFactory jfactory = new JsonFactory();
    
    private static ObjectMapper objectMapper = new ObjectMapper();
    
    /**
     * 
     * java对象转为json字符串 （或者是java集合对象转为json数组字符串）
     * 
     * @param obj
     * @return
     */
    public static String readObject2Json(Object obj)
    {
        try
        {
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * json字符串转为对象 <功能详细描述>
     * 
     * @param json
     * @param c
     * @return
     */
    public static <T> T readJson2Entity(String json, Class<?> c)
    {
        if (json == null)
        {
            return null;
        }
        try
        {
            return (T)objectMapper.readValue(json, c);
        }
        catch (JsonParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (JsonMappingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * json数组字符串转为集合类对象 如ArrayList<Map> <功能详细描述>
     * 
     * @param jsonArr
     * @param collectionClass
     * @param elementClasses
     * @return
     */
    public static <T> T readJsonArray2Collection(String jsonArr, Class<?> collectionClass, Class<?>... elementClasses)
    {
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
        try
        {
            return objectMapper.readValue(jsonArr, javaType);
        }
        catch (JsonParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (JsonMappingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    public static void main(String args[])
    {
        // json转对象
        String json = "{\"address\":\"address\",\"name\":\"haha\",\"id\":1,\"email\":\"email\"}";
        Map map = (Map)readJson2Entity(json, HashMap.class);
        System.out.println(map);
        System.out.println(map.get("email"));
        // 对象转json
        System.out.println("readObject2Json::" + readObject2Json(map));
        // json数组转集合
        String jsonArr =
            "[{\"address\":\"address\",\"name\":\"haha\",\"id\":1,\"email\":\"email\"},{\"address\":\"address2\",\"name\":\"haha2\",\"id\":2,\"email\":\"email2\"}]";
        List<Map> list = readJsonArray2Collection(jsonArr, ArrayList.class, HashMap.class);
        
        for (Map m : list)
        {
            System.out.println("readJsonArray2Collection::" + m);
            System.out.println("m.getname::" + m.get("name"));
        }
        // 集合转json数组
        String jsonArr2 = readObject2Json(list);
        System.out.println("jsonArr2::" + jsonArr2);
    }
}
