package com.mongodb.service;

import com.mongodb.model.Collection;
import com.mongodb.model.DataBase;
import org.dom4j.Element;

import java.util.List;
import java.util.Map;

/**
 * Created by chrhc on 2016/5/23.
 */
public interface IMDataBaseService {
    //public List<DataBase> showDataBase();
    //public void setCurrentDb(DataBase currentDb);
    //public DataBase createDataBase(String name);

   // public boolean dropDataBase(String name);
    public Collection createCollection(String name);
    public Collection dropCollection(String name);
    public List<Collection> showCollection();
    public Collection userCollection(String name);
}
