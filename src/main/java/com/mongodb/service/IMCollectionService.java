package com.mongodb.service;

import java.util.List;
import java.util.Map;

/**
 * Created by chrhc on 2016/5/23.
 */
public interface IMCollectionService {
    public int insert(Map data);
    public int update(Map data);
    public int delete(Map data);
    public List<Map> find();
}
