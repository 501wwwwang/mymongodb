package com.mongodb.service;

import com.mongodb.model.Collection;
import com.mongodb.model.DataBase;
import com.mongodb.model.Root;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by chrhc on 2016/5/23.
 */
public class MDataBaseService implements IMDataBaseService{
   /* private  DataBase currentDb ;
    private  List<DataBase> dataBases;*/
    private  DataBase currentDataBase ;
    private Collection currentCollection;
    private Root root;

    public MDataBaseService(Root root,DataBase currentDataBase){
        this.root = root;
        this.currentDataBase = currentDataBase;
    }


    @Override
    public Collection createCollection(String name) {


        Collection collection = new Collection(name);
        currentDataBase.addChild(collection);
        return collection;
    }

    @Override
    public Collection dropCollection(String name) {
        return currentDataBase.removeChild(name);
    }

    @Override
    public List<Collection> showCollection() {
        return currentDataBase.getCollections();
    }

    @Override
    public Collection userCollection(String name){
        List<Collection> collections = currentDataBase.getCollections();
        for (Collection col : collections) {
            if(col.getName().equals(name)){
                this.currentCollection = col;
                break;
            }
        }
        if(currentCollection == null){
            currentCollection = new Collection();
            currentCollection.setName(name);
            currentDataBase.addChild(currentCollection);
        }
        return currentCollection;
    }
}
