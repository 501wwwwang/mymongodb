package com.mongodb.service;

import com.mongodb.model.BaseCompose;
import org.dom4j.Element;

import java.util.Map;

/**
 * Created by chrhc on 2016/5/23.
 */
public interface ICompose {
    public boolean loadXmlDataBase(Element node);

}
