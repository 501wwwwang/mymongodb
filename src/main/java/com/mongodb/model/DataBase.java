package com.mongodb.model;

import com.mongodb.service.ICompose;
import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

import java.util.*;

/**
 * Created by chrhc on 2016/5/23.
 */
public class DataBase extends BaseCompose  implements ICompose {
    private String name;
    private List<Collection> collections = new ArrayList();
private Logger logger = Logger.getLogger(DataBase.class);

    public  DataBase(){

    }
    public  DataBase(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Collection> getCollections() {
        return collections;
    }

    public void setCollections(List<Collection> collections) {
        this.collections = collections;
    }



    public boolean loadXmlDataBase(Element node) {
        //String nodeName = node.getName();
        Map tem = new HashMap();
        // 获取当前节点的所有属性节点
        List<Attribute> list = node.attributes();
        // 遍历属性节点
        for (Attribute attr : list)
        {
            tem.put(attr.getName(), attr.getValue());
        }
        setName((String)tem.get("name"));

        Iterator it = node.elementIterator();
        String nodeName = null;
        while(it.hasNext()){
            Element e = (Element) it.next();
            nodeName = e.getName();
            if(nodeName!=null &&nodeName.equals("collection")){
                Collection collection = new Collection();
                addChild(collection);
                collection.loadXmlDataBase(e);
            }
        }

        return false;
    }

    @Override
    public void addChild(BaseCompose compose) {
        Collection cc = (Collection)compose;
        for (int i = collections.size()-1;i>=0;i--) {
            Collection table = collections.get(i);
            if(table.getName().equals(cc.getName())){
              logger.error("已经存在相同表名:"+table.getName());
                return;
            }
        }
        collections.add((Collection)compose);
    }
    public Collection removeChild(String name){
        for (int i = collections.size()-1;i>=0;i--) {
            Collection table = collections.get(i);
            if(table.getName().equals(name)){
                table.removeChildAll();
                collections.remove(i);
                return table;
            }
        }
        return  null;
    }

    public int removeChildAll(){
        int n = 0;
        for (int i = collections.size()-1;i>=0;i--) {
            Collection table = collections.get(i);
            table.removeChildAll();
            collections.remove(i);
            n++;
        }
        return  n;
    }


    public void witeXmlDataBase(Element databases){
        Element database = databases.addElement("database");
        logger.debug("Database write name:"+name);
        database.addAttribute("name",name);
        for (Collection collection: collections) {
            collection.witeXmlDataBase(database);
        }
    }


}
