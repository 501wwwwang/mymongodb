package com.mongodb.model;

import com.mongodb.service.ICompose;
import com.mongodb.service.IMCollectionService;
import com.mongodb.util.UtilJson;
import org.dom4j.Attribute;
import org.dom4j.Element;

import java.util.*;

/**
 * Created by chrhc on 2016/5/23.
 */
public class Collection extends BaseCompose implements ICompose, IMCollectionService {
    private  String name;
    private  List<Map> data = new ArrayList<Map>();

    public List<Map> getData() {
        return data;
    }

    public void setData(List<Map> data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public  Collection(){

    }
    public  Collection(String name){
        this.name = name;
    }

    public boolean loadXmlDataBase(Element node) {
        Map tem = new HashMap();
        // 获取当前节点的所有属性节点
        List<Attribute> list = node.attributes();
        // 遍历属性节点
        for (Attribute attr : list)
        {
            tem.put(attr.getName(), attr.getValue());
        }
        setName((String)tem.get("name"));
        Iterator it = node.elementIterator();
        String nodeName = null;
        while(it.hasNext()){
            Element e = (Element) it.next();
            nodeName = e.getName();
            if(nodeName!=null &&nodeName.equals("data")){
                String val = e.getText();
                Map  data = UtilJson.readJson2Entity(val,HashMap.class);
                addChild(data);

            }
        }

        return true;
    }

    @Override
    public void addChild(BaseCompose compose) {
    }

    public void addChild(Map d) {
        data.add(d);
    }

    public Map removeChild(String id){
        Map m =null;
        for (int i = data.size()-1;i>=0;i--){
            m = data.get(i);
            if(complareData(m,id)){
                data.remove(i);
                return m;
            }
        }
        return null;
    }

    private boolean complareData(Map m,String id){
        String _id = (String) m.get("_id");
        if(id.equals(_id)){
            return true;
        }
        return  false;
    }

    private boolean complareData(Map m,Map d){
        String _id = (String) m.get("_id");
        String _id2 = (String) d.get("_id");
        if(_id2.equals(_id)){
            return true;
        }
        return  false;
    }

    public void removeChildAll(){
        data.clear();
    }

    public void witeXmlDataBase(Element database){
        Element collection = database.addElement("collection");
        collection.addAttribute("name",name);
        for (Map d: data) {
            Element dataEle = collection.addElement("data");
            dataEle.setText(UtilJson.readObject2Json(d));
        }
    }

    @Override
    public int insert(Map data) {
        this.addChild(data);
        return 1;
    }

    @Override
    public int update(Map d) {
        Map m ;
        for (int i = data.size()-1; i >= 0; i--) {
            m = data.get(i);
            if(complareData(m,d)){
                m.putAll(d);

            }
        }
        return 1;
    }

    @Override
    public int delete(Map d) {
        Map m ;
        for (int i = data.size()-1; i >= 0; i--) {
            m = data.get(i);
            if(complareData(m,d)){
               data.remove(m);

            }
        }
        return 1;
    }

    @Override
    public List<Map> find() {
        return data;
    }
}
