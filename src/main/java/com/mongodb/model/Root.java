package com.mongodb.model;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;

import java.util.*;

/**
 * Created by chrhc on 2016/5/23.
 */
public class Root extends BaseCompose {
    List<DataBase> dataBases =new ArrayList<DataBase>();

    public List<DataBase> getDataBases() {
        return dataBases;
    }

    public void setDataBases(List<DataBase> dataBases) {
        this.dataBases = dataBases;
    }

    @Override
    public void addChild(BaseCompose compose) {
        dataBases.add((DataBase)compose);
    }

    public DataBase removeChild(String name){
        for (int i = dataBases.size()-1;i>=0;i--) {
            DataBase db = dataBases.get(i);
            if(db.getName().equals(name)){
                db.removeChildAll();
                dataBases.remove(i);
                return db;
            }
        }
        return  null;
    }

    public void loadXmlDataBase(Element node){
        String nodeName = node.getName();
        if (nodeName.equals("database")){
            DataBase db = new DataBase();
//            db.setName((String)tem.get("name"));
            addChild(db);
            db.loadXmlDataBase(node);

        }

    }

    public void witeXmlDataBase(Document doc){
        Element node =doc.addElement("databases");
        for (DataBase db: dataBases) {
            System.out.println("root db Name:"+db.getName());
            db.witeXmlDataBase(node);
        }
    }
}
