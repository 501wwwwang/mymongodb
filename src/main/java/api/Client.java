package api;

import com.mongodb.MongodbServer;
import com.mongodb.model.Collection;
import com.mongodb.service.IMDataBaseService;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chrhc on 2016/5/24.
 */
public class Client {


    public static void main(String args[]) {
        MongodbServer server = new MongodbServer();
        server.init("D:/mongodb/text.xml");
        //数据库展示
        server.showDataBase();
        //使用数据库展示
        IMDataBaseService fool = server.useDataBase("fool");
        fool.createCollection("user1");
        fool.createCollection("user2");
        Collection person = fool.userCollection("person");
        List<Collection> foolCols = fool.showCollection();

        //dataBaseService.
        //数据插入展示
        Map d = new HashMap();
        d.put("_id","14");
        d.put("name","wang124");
        person.insert(d);
        Map d2 = new HashMap();
        d2.put("_id","15");
        d2.put("name","wang123455");
        person.insert(d2);

        Collection user1 = fool.userCollection("user1");
        Map u1 = new HashMap();
        u1.put("_id","1");
        u1.put("name","wang123455");
        user1.insert(u1);

        //内存同步磁盘文件展示
        try {
            server.witeXmlDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
